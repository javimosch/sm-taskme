
import './components/sidebar'
import './components/appFooter'
import './components/boardsList'

import Dashboard from './containers/dashboard'
import AddBoard from './containers/addBoard'

window.ERROR = {
    API: 'Erreur de serveur ou de connexion'
}
const routes = [
    { path: '/', component: Dashboard, name: 'dashboard' },
    { path: '/add-board', component: AddBoard, name: 'addBoard' },
]
const router = new VueRouter({
    routes
})

// window.router = router
window.onLogout = () => {
    router.push({name:'logout'})
}
window.onLogin = () => {
    router.push({name:'dashboard'})
}

new Vue({
    name: 'app',
    el: '.app',
    router,
    data() {
        return {}
    },
    created() {
        console.log('APP created')
    }
})

console.log('APP')