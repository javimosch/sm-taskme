module.exports = app => {
    var debug = require('debug')(`app:api:selectQuery ${`${Date.now()}`.white}`)
    return async function selectQuery(data) {
        let creation_date = require('moment-timezone')().tz('Europe/Paris')._d.getTime()
        return await app.dbExecute(
            `
SELECT ${data.query}
    `,
    data.queryArgs||[],
            {
                dbName: this.dbName
            }
        )
    }
}