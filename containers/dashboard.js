import stylesMixin from '../mixins/styles'

export default {
    mixins: [stylesMixin],
    name: 'dashboard',
    props: [],
    template: `
    <div ref="scope">
        <div class="dashboard" ref="root" >
            <h2>Tableau de bord</h2>
            <boards-list></boards-list>
        </div>
    </div>
    `,
    data() {
        var self = this
        return {
            styles: `
            .dashboard{
            
            }
            
            @media only screen and (max-width: 639px) {
                
            }`
        }
    },
    computed: {
       
    },
    methods: {
       async addBoard(){
           await api.funql({
               name:'taskmeBoardAdd',args:[{
                   name:"BOARD TEST"
               }]
           })
       }
    },
    mounted() {
        
    }
}